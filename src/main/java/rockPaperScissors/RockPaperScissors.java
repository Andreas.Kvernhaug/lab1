package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        //String roundStart = "Let's play round "
        String choisePrompt = "Your choice (Rock/Paper/Scissors)?";

        boolean play = true;
        while (play == true){
            System.out.printf("Let's play round %d\n", roundCounter);
            roundCounter += 1;
            // Player chooses rock, paper or scissors
            String humanChoise = readInput(choisePrompt);
            // Force input to lower case
            humanChoise = humanChoise.toLowerCase();
            int humanChoiseIndex = rpsChoices.indexOf(humanChoise);

            int computerChoiseIndex = randomChoise();
            String computerChoise = rpsChoices.get(computerChoiseIndex);

            boolean legalChoise = rpsChoices.contains(humanChoise);
            if (legalChoise == false){
                System.out.println("Input error");
            }

            else if (humanChoiseIndex == computerChoiseIndex){
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", humanChoise, computerChoise);
            }

            else if (humanChoiseIndex == computerChoiseIndex + 1 || (humanChoiseIndex == 0 && computerChoiseIndex == 2)){
                humanScore += 1;
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", humanChoise, computerChoise);
            }

            else {
                computerScore += 1;
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", humanChoise, computerChoise);
            }
            
            // Show scores
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            // Ask if player wants to play again
            String replayPrompt = "Do you wish to continue playing? (y/n)?";
            String replay = readInput(replayPrompt);

            if (replay.equals("n")){
                // exit while loop
                System.out.println("Bye bye :)");
                play = false;
            } 
            

            

            

        }
            

    }

    public int randomChoise(){
        Random rand = new Random();
        int computerChoiseIndex = rand.nextInt(rpsChoices.size());

        return computerChoiseIndex;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
